﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MultiplayerQuiz.Models
{
    public class NewQuestionModel
    {
        [Required]
        [MaxLength(100)]
        [Display(Name="Question (max 100 chars)")]
        public string QuestionText { get; set; }

        [MaxLength(500)]
        [Display(Name = "Explanation (max 500 chars)")]
        public string Description { get; set; }

        [Required]
        [MaxLength(100)]
        [Display(Name = "Correct Option (max 100 chars)")]
        public string CorrectOption { get; set; }

        [Required]
        [MaxLength(100)]
        [Display(Name = "Incorrect Option 1 (max 100 chars)")]
        public string IncorrectOption1 { get; set; }

        [Required]
        [MaxLength(100)]
        [Display(Name = "Incorrect Option 2 (max 100 chars)")]
        public string IncorrectOption2 { get; set; }

        [Required]
        [MaxLength(100)]
        [Display(Name = "Incorrect Option 3 (max 100 chars)")]
        public string IncorrectOption3 { get; set; }

        [Required(ErrorMessage="You need to provide a reference")]
        [MaxLength(500)]
        [Display(Name = "Any reference? Where can we go to check your question? (max 500 chars)")]
        public string ReferenceUrl { get; set; }

        public string EmailAddress { get; set; }

        public string Username { get; set; }
    }
}