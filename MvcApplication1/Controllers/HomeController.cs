﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MultiplayerQuiz.Models;
using System.Threading.Tasks;
using SignalR.Client.Hubs;

namespace MultiplayerQuiz.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to ASP.NET MVC!";

            return View();
        }

        [HttpPost]
        public ActionResult AddQuestion(NewQuestionModel newQuestion)
        {
            if (!ModelState.IsValid)
            {
                return View("Index", newQuestion);
            }

            System.Net.ServicePointManager.Expect100Continue = false;

            var gameConnection = new HubConnection("http://www.williamb.net");
            var gameProxy = gameConnection.CreateProxy("Gameshow.Server.SignalR.GameShow");

            var connected = gameConnection.Start().Wait(60000);

            gameProxy.Invoke("AddQuestion", newQuestion.QuestionText, newQuestion.Description, newQuestion.CorrectOption, newQuestion.IncorrectOption1, newQuestion.IncorrectOption2, newQuestion.IncorrectOption3, newQuestion.ReferenceUrl).Wait();

            gameConnection.Stop();



            var appHubGameConnection = new HubConnection("http://MultiplayerQuiz.apphb.com");
            var appHubGameProxy = appHubGameConnection.CreateProxy("Gameshow.Server.SignalR.GameShow");

            var appHubConnected = appHubGameConnection.Start().Wait(60000);

            appHubGameProxy.Invoke("AddQuestion", newQuestion.QuestionText, newQuestion.Description, newQuestion.CorrectOption, newQuestion.IncorrectOption1, newQuestion.IncorrectOption2, newQuestion.IncorrectOption3, newQuestion.ReferenceUrl).Wait();

            appHubGameConnection.Stop();

            return RedirectToAction("QuestionAdded");
        }

        public ActionResult QuestionAdded()
        {
            return View();
        }

        public ActionResult Faq()
        {
            return View();
        }
    }
}
